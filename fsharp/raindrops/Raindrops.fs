﻿module Raindrops

let pling x =
    match x with
    | x when x % 3 = 0 -> "Pling"
    | x -> ""

let plang x =
    match x with
    | _ when x % 5 = 0 -> "Plang"
    | _ -> ""

let plong x =
    match x with
    | _ when x % 7 = 0 -> "Plong"
    | _ -> ""
    

let convert (number: int): string = 
    let names = [ pling; plang; plong ] |> List.map (fun f -> f number)
    let name = names |> List.reduce ( fun s t -> s + t )

    match name with
    | "" -> number.ToString()
    | _ -> name