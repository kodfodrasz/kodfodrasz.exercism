﻿module QueenAttack

let create (position: int * int) = 
    match position with
    | x, _ when x < 0 || x >= 8 -> false
    | _, y when y < 0 || y >=8 -> false
    | x, y -> true

let translatePosition (origin: int * int) (displacement: int * int) = 
    let x, y = origin
    let dx, dy = displacement

    (x + dx, y + dy)

let translateAndValidatePosition (origin: int * int) (displacement: int * int) = 
    let translated = translatePosition origin displacement

    match translated with
    | t when t |> create -> Some (translated, translated)
    | _ -> None

let hits (queen: int * int) = 
    let left   = queen |> Seq.unfold( fun pos -> translateAndValidatePosition pos (-1,0) )
    let right  = queen |> Seq.unfold( fun pos -> translateAndValidatePosition pos (1,0) )
    let top    = queen |> Seq.unfold( fun pos -> translateAndValidatePosition pos (0,-1) )
    let bottom = queen |> Seq.unfold( fun pos -> translateAndValidatePosition pos (0,1) )

    let diagonalTopLeft     = queen |> Seq.unfold( fun pos -> translateAndValidatePosition pos (-1,-1) )
    let diagonalTopRight    = queen |> Seq.unfold( fun pos -> translateAndValidatePosition pos (1,-1) )
    let diagonalBottomLeft  = queen |> Seq.unfold( fun pos -> translateAndValidatePosition pos (-1,1) )
    let diagonalBottomRight = queen |> Seq.unfold( fun pos -> translateAndValidatePosition pos (1,1) )

    seq {
        yield queen
        yield! left
        yield! right
        yield! top
        yield! bottom
        yield! diagonalTopLeft     
        yield! diagonalTopRight    
        yield! diagonalBottomLeft  
        yield! diagonalBottomRight 
    }
    

let canAttack (queen1: int * int) (queen2: int * int) = 
    let q1Hits = hits queen1
    q1Hits |> Seq.contains queen2 
