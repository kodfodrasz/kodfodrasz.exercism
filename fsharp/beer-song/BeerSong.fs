﻿module BeerSong


let recite (startBottles: int) (takeDown: int) = 
    let fullCount = 99

    let pluralHeader i = sprintf "%i bottles of beer on the wall, %i bottles of beer." i i
    let singularHeader i = sprintf "%i bottle of beer on the wall, %i bottle of beer." i i
    
    let pluralTail i = sprintf "Take one down and pass it around, %i bottles of beer on the wall." i
    let singularTrail i = sprintf "Take one down and pass it around, %i bottle of beer on the wall." i

    let verse i = 
        match i with
        | 0 -> 
            [
                "No more bottles of beer on the wall, no more bottles of beer.";
                sprintf "Go to the store and buy some more, %i bottles of beer on the wall." fullCount
            ]
        | 1 ->
            [
            (singularHeader 1);
            "Take it down and pass it around, no more bottles of beer on the wall.";
            ]
        | 2 ->
            [
            (pluralHeader 2);
            (singularTrail 1)
            ]
        | more ->
            [
                (pluralHeader more);
                (pluralTail (more - 1));
            ]

    let bottles = seq { startBottles .. -1 .. 0 } |> Seq.truncate takeDown

    let verses = 
        bottles
        |> Seq.map verse

    let head = Seq.head verses    
    Seq.tail verses
    |> Seq.fold (fun acc i -> List.concat [acc; [""] ; i] ) head
    |> Seq.toList