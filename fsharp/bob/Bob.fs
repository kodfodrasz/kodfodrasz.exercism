﻿module Bob

open System


let isEmpty = String.IsNullOrWhiteSpace
let isQuestion (str:string) = str.Trim().EndsWith('?')
let isYell (str:string) = 
    let trimmed = str.Trim()

    let letters = 
        trimmed
        |> Seq.filter Char.IsLetter
        |> Seq.toList

    not(letters.IsEmpty) && (Seq.forall Char.IsUpper letters)

let isYellQuestion (str:string) = isQuestion str && isYell str

let (|YellQuestion|Question|Yell|Empty|Other|) str =
    if str |> isEmpty then Empty
    elif str |> isYellQuestion then YellQuestion
    elif str |> isYell then Yell
    elif str |> isQuestion then Question
    else Other


let response (input: string): string = 
    match input with 
    | Question      -> "Sure."
    | Yell          -> "Whoa, chill out!"
    | YellQuestion  -> "Calm down, I know what I'm doing!"
    | Empty         -> "Fine. Be that way!"
    | _             -> "Whatever."