// This file was auto-generated based on version 1.1.0 of the canonical data.

module SpiralMatrixMyTests

open FsUnit.Xunit
open Xunit

open SpiralMatrix

 
// ### My tests ###

// TODO: how to write more readable code for lazy sequence assertions?

// Step spans
[<Fact>]
let ``Step spans 0 (empty)`` () =
    stepSpan 0 
        |> should equal Seq.empty<int> 

[<Fact>]
let ``Step spans 1`` () =
    stepSpan 1 
        |> Seq.toList 
        |> should equal [ 0 ] 

[<Fact>]
let ``Step spans 2`` () =
    stepSpan 2 
        |> Seq.toList 
        |> should equal  [ 1; 1; 1 ] 

[<Fact>]
let ``Step spans 3`` () =
    stepSpan 3 
        |> Seq.toList 
        |> should equal  [ 2; 2; 2; 1; 1 ] 

[<Fact>]
let ``Step spans 4`` () =
    stepSpan 4 
        |> Seq.toList 
        |> should equal  [ 3; 3; 3; 2; 2; 1; 1 ] 

[<Fact>]
let ``Step spans 5`` () =
    stepSpan 5 
        |> Seq.toList 
        |> should equal [ 4; 4; 4; 3; 3; 2; 2; 1; 1 ]

// Steps
[<Fact>]
let ``Steps 0 (empty)`` () =
    steps 0 
        |> Seq.toList
        |> should equal ([ ] : Step list)

[<Fact>]
let ``Steps 1`` () =
    steps 1 
        |> Seq.toList 
        |> should equal ([ ] : Step list) 

[<Fact>]
let ``Steps 5`` () =
    steps 5 
        |> Seq.toList 
        |> should equal 
        [ 
            Right; Right; Right; Right;
            Down; Down; Down; Down; 
            Left; Left; Left; Left;
            Up; Up; Up; 
            Right; Right; Right; 
            Down; Down;
            Left; Left;
            Up;
            Right;
        ] 

// Indices
[<Fact>]
let ``Indices 0 (empty)`` () =
    indices 0 
        |> Seq.toList 
        |> should equal ([] : (int*int) list)

[<Fact>]
let ``Indices 1`` () =
    indices 1 
        |> Seq.toList 
        |> should equal [ (0,0) ] 

[<Fact>]
let ``Indices 5`` () =
    indices 5
        |> Seq.toList 
        |> should equal 
        [ 
            // Right; Right; Right; Right; Right;
            (0,0); (1,0); (2,0); (3,0); (4,0);
            // Down; Down; Down; Down; 
            (4,1); (4,2); (4,3); (4,4);
            // Left; Left; Left; Left;
            (3,4); (2,4); (1,4); (0,4);
            // Up; Up; Up; 
            (0,3); (0,2); (0,1);
            // Right; Right; Right; 
            (1,1); (2,1); (3,1);
            // Down; Down;
            (3,2); (3,3);
            // Left; Left;
            (2,3); (1,3);
            // Up;
            (1,2);
            // Right;
            (2,2)
        ] 
