﻿module SpiralMatrix

type Step = 
    | Up
    | Down 
    | Left 
    | Right 

let stepSpan size =
    if size = 0 then Seq.empty<int> else

    let mutable s = (size - 1)
    seq {
        yield s
        while ( 0 < s ) do
            yield s
            yield s
            s <- s-1
    }

let steps size = 
    let directions = seq { while true do yield! [ Right; Down; Left; Up ] }
    let spans = stepSpan size

    Seq.zip spans directions 
        |> Seq.collect (function (s, d) -> seq { for _ in 1..s do yield d })

let indices size =
    if size = 0 then Seq.empty else

    let folder (x, y) dir = 
        match dir with
            | Left  -> (x-1, y)
            | Right -> (x+1, y)
            | Up    -> (x  , y-1)
            | Down  -> (x  , y+1)

    steps size |> Seq.scan folder (0,0)

let spiralMatrix size : int list list = 
    if size = 0 then [] else
    
    let matrix = [| for i in 1..size -> Array.zeroCreate size |]

    let items = Seq.initInfinite (function i -> i) |> Seq.skip 1

    let assign (x, y) item = 
        matrix.[y].[x] <- item

    Seq.iter2 assign (indices size) items

    matrix 
        |> Array.map Array.toList 
        |> Array.toList
        
