﻿module DndCharacter

open System

let private rng = new Random()

type Dice = Dice of int

let d sides = Dice sides

let roll count dice =
    let throw dice = match dice with Dice sides -> 1 + rng.Next(sides)

    [ for i in 1..count -> throw dice ]    

let best count rolls =
    rolls |> List.sortByDescending (fun i->i) |> List.take count

type Character = {
    Strength     : int;
    Dexterity    : int;
    Constitution : int; 
    Intelligence : int;
    Wisdom       : int;
    Charisma     : int;
    Hitpoints    : int;
}

let modifier x =
    ((float)x - 10.0)/2.0 |> Math.Floor |> int

let ability() = 
    let rolls = roll 4 (d 6) 
    List.sum <| best 3 rolls

let createCharacter() : Character =
    let constitution = ability();
    {
        Strength     = ability();
        Dexterity    = ability();
        Constitution = constitution;
        Intelligence = ability();
        Wisdom       = ability();
        Charisma     = ability();
        Hitpoints    = 10 + (constitution |> modifier)
    }
