﻿module Sieve

let primes limit  =
    let numbers =
        Seq.initInfinite (fun i->i) 
        |> Seq.skip 2 
        |> Seq.takeWhile (fun x ->  x<limit)

    let sieve n series = 
        seq {
            for num in series do 
            if num % n <> 0 then 
                yield num
        }

    let rec eratosthenes acc nums =
        if Seq.isEmpty nums then acc |> Seq.rev
        else

        let n = Seq.head nums
        let rest = Seq.tail nums

        let filtered = sieve n rest
        
        eratosthenes (n :: acc) filtered

    eratosthenes [] numbers