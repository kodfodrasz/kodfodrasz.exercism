﻿module ForthTestsMine

open FsUnit.Xunit
open Xunit

open Forth

[<Fact>]
let ``Tokinizer Tokenizes every token type correctly`` () =
    let expected = [
        IntegerOpPlus;
        IntegerOpMinus;
        IntegerOpMultiply;
        IntegerOpDivide;
        IntegerLiteral 1;
        IntegerLiteral -2;
        CustomWordDefinitionStart
        CustomWordName "1CUSTOM";
        CustomWordName "CUSTOM";
        StackOpDup;
        StackOpDrop;
        StackOpSwap;
        StackOpOver;
        CustomWordDefinitionEnd;
    ]
    let result = tokenize "+ - * / 1 -2 : 1custom
    custom DUP DROP SWAP OVER ; " 
    
    result |> should equal expected

[<Fact>]
let ``Tokenization works for User-defined words - can consist of built-in words`` () =
    let expected = [
        CustomWordDefinitionStart ;
        CustomWordName "DUP-TWICE";
        StackOpDup;
        StackOpDup;
        CustomWordDefinitionEnd;
        ]
    tokenize ": dup-twice dup dup ;" |> should equal expected

[<Fact>]
let ``User-defined words - evaluation continues after evaluatin user defined words`` () =
    let expected = Some [1; 2; 3]
    evaluate [": foo 2 ;"; "1 foo 3"] |> should equal expected