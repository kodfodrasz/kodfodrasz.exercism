﻿module Forth

type Token = 
    | IntegerLiteral of int
    | WordDefinitionStart
    | WordName of string
    | WordDefinitionEnd

type Stack = int list
type FMachine = {
    Stack : Stack;
    Words : Map<string, List<Opcode>>;
}
and Opcode = 
    | PushInt  of int
    | CallWord of string
    | DefnWord of string * List<Opcode>
    | Builtin  of string * (Stack -> Stack)

    
let tokenize input = 
    let regex r =  new System.Text.RegularExpressions.Regex(r)
    
    let lexer = regex @"((?<lexeme>(\w[\w-_]*|-?\d+|[+-/*;:]))\s*)*"
    let matches = lexer.Match(input)

    [ for l in matches.Groups.["lexeme"].Captures do 
        let lexeme = l.Value.ToUpper() 
        let token = 
            match lexeme with
            | ":"    -> WordDefinitionStart
            | ";"    -> WordDefinitionEnd
            | lexeme -> match System.Int32.TryParse lexeme with
                        | true, intval -> IntegerLiteral intval
                        | false, _     -> WordName lexeme
        yield token
    ]

let parse tokens =
    let lookahead tokens = 
        let isTerminator t = match t with
                                | WordDefinitionEnd -> true
                                | _ -> false

        let idx = List.findIndex isTerminator tokens

        let lead = List.take idx tokens
        let tail = List.skip (idx + 1) tokens

        lead, tail

    let rec parseTokens ops tokens : List<Opcode> =
        match tokens with
        | [] -> List.rev ops
        | IntegerLiteral i    :: rest -> parseTokens (PushInt i :: ops) rest
        | WordName word       :: rest -> parseTokens (CallWord word :: ops) rest
        | WordDefinitionStart :: rest ->
            match lookahead rest with
            | (WordName word :: defn), rest -> 
                let parsedDefn = parseTokens [] defn
                parseTokens (DefnWord (word, parsedDefn):: ops) rest
            | _ -> failwith "Expected a word name after word definition operator"
        | WordDefinitionEnd :: _ -> failwith "Unexpected word definition terminator" // lookahead removes terminators for well-formed word definitions
        
    parseTokens [] tokens

let rec execute machine ops = 
    let inlineWords (words : Map<string,List<Opcode>>) defn = 
        let inliner op = 
            match op with
            | DefnWord (_,_)-> failwith "Unexpected nested word definition!" // cannot happen due to how lookahead operates!
            | CallWord word -> words.[word]
            | _             -> [ op ]

        defn |> List.collect inliner

    let rec evalOp machine op = 
        match op with
        | PushInt i             -> ({machine with Stack = i :: machine.Stack}, [])
        | DefnWord (word, defn) -> let inlined = inlineWords machine.Words defn
                                   ({machine with Words = machine.Words.Add(word, inlined) }, [])
        | CallWord word         -> let defn = machine.Words.[word]
                                   (machine, defn)
        | Builtin (word, code)  -> ({machine with Stack = code machine.Stack}, [])

    match ops with
    | [] -> machine
    | op :: rest -> let machine2, subroutine = evalOp machine op
                    execute machine2 <| List.append subroutine rest

let evaluate input =
    let InitBuiltinWord (word: string) f =
        let upper  = word.ToUpperInvariant()
        upper, [ Builtin (upper, f word) ]

    let BinaryOp f name stack = 
        match stack with
        | a :: b :: rest -> (f b a) :: rest
        | _ -> failwith <| sprintf "Binary operator '%s' needs at least 2 items on the stack" name
    
    let StackManipulationOp f name stack = 
        match stack with
        | a :: rest -> f a rest
        | _ -> failwith <| sprintf "Unary stack operator '%s' needs at least 1 item on the stack" name

    let StackManipulationBinaryOp f name stack = 
        match stack with
        | a :: b :: rest -> f a b rest
        | _ -> failwith <| sprintf "Binary stack operator '%s' needs at least 2 items on the stack" name
        
    let machine = { Stack = []; Words = Map.ofList [
        InitBuiltinWord "+"    <| BinaryOp (+);
        InitBuiltinWord "-"    <| BinaryOp (-);
        InitBuiltinWord "*"    <| BinaryOp (*);
        InitBuiltinWord "/"    <| BinaryOp (/);
        InitBuiltinWord "DUP"  <| StackManipulationOp (fun a rest -> a::a::rest)
        InitBuiltinWord "DROP" <| StackManipulationOp (fun a rest -> rest)
        InitBuiltinWord "SWAP" <| StackManipulationBinaryOp (fun a b rest -> b::a::rest)
        InitBuiltinWord "OVER" <| StackManipulationBinaryOp (fun a b rest -> b::a::b::rest)
    ]}

    try
        let machine = 
            input 
            |> List.collect (tokenize >> parse)
            |> execute machine
        Some ( machine.Stack |> List.rev)
    with
    | _ -> None
