﻿module Accumulate

let accumulate (func: 'a -> 'b) (input: 'a list): 'b list = 
    let rec map acc func input = 
        match input with
        | [] -> acc
        | h :: t ->  map (func h :: acc) func t

    map [] func input |> List.rev