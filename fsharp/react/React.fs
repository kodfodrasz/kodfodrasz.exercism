﻿module React

open System
open Microsoft.FSharp.Control

module Graph =

    type Node<'T> = Node of data:'T

    type Edge<'T> = Edge of origin:Node<'T> * target:Node<'T>

    let origin edge = match edge with Edge(origin, target) -> origin 
    let target edge = match edge with Edge(origin, target) -> target 

    let topologicalSort nodes edges = 
        let isNodeSatisfiedIn edges node = 
            edges |> Seq.forall (fun e -> (target e) <> node)

        let edgeFrom node edge = (origin edge) = node        

        let rec kahn acc readyNodes remainingEdges = 
            match readyNodes with
            | []            ->  if remainingEdges |> List.isEmpty 
                                then acc |> List.rev |> Some
                                else None // Not a DAG!
            | node :: rest  ->  let satisfiedEdges = remainingEdges |> List.filter (edgeFrom node)
                                let pendingEdges = remainingEdges |> List.except satisfiedEdges
                                let candidates = satisfiedEdges |> List.map target

                                let satisfiedNodes = 
                                    candidates 
                                    |> List.filter (isNodeSatisfiedIn pendingEdges)
                            
                                kahn (node :: acc) (rest @ satisfiedNodes) pendingEdges

                            
        let startNodes = nodes |> Set.filter (isNodeSatisfiedIn edges) |> Set.toList

        kahn [] startNodes (edges |> Set.toList)


type ICell =
    abstract member Value : int
    abstract member Changed : IEvent<int>
    abstract member Refresh : Unit -> Unit
    abstract member Dependencies : ICell list

    inherit IComparable

#nowarn "343"
#nowarn "25"

type InputCell (value) =
    let mutable value = value
    let changed = new Event<int>()

    member this.Value with get() = value and set v = value <- v; changed.Trigger(v)
    member this.Changed = changed.Publish

    interface ICell with
        member this.Value = this.Value
        member this.Changed = this.Changed
        member this.Refresh() = ()
        member this.Dependencies = []
    interface IComparable with
        member this.CompareTo that = this.GetHashCode() - that.GetHashCode()

type ComputeCell (inputs: ICell list, expr: int list -> int) = 
    let compute() = 
        inputs
        |> List.map (fun i -> i.Value)
        |> expr

    let mutable value = compute()
    let changed = new Event<int>()
    let refresh() =
        let newvalue = compute()
        if newvalue <> value then
            value <- newvalue
            changed.Trigger(newvalue)        

    member this.Value = value
    member this.Changed = changed.Publish

    interface ICell with
        member this.Value = value
        member this.Changed = changed.Publish   
        member this.Refresh() = refresh()
        member this.Dependencies = inputs
    interface IComparable with
        member this.CompareTo that = this.GetHashCode() - that.GetHashCode()

type Reactor() = 
    let mutable cells = List.empty<ICell>

    let calculateOrder () =
        let nodes = cells |> List.map Graph.Node

        let nodeOf cell = nodes |> List.find (fun n -> match n with Graph.Node data -> data = cell)

        let edges = 
            cells 
            |> List.collect (fun cell ->
                let cellNode = nodeOf cell
                cell.Dependencies 
                |> List.map (fun dep -> Graph.Edge( nodeOf dep , cellNode)))
        
        let sorted =  Graph.topologicalSort (Set.ofList nodes) (Set.ofList edges)
        Option.get sorted // ugly

    let mutable order = Lazy<List<Graph.Node<ICell>>>( calculateOrder )
    let mutable dirtyCells = List.empty<ICell>
    
    let doUpdate() = 
        while not dirtyCells.IsEmpty do
            let cell :: rest = dirtyCells
            // dirtycells needs to be updated first, as Refresh potentially also mutates dirtyCells!
            dirtyCells <- rest
            cell.Refresh()

    let scheduleDependentCellUpdate (cell : ICell) value = 
        let dependents = cells |> List.filter (fun c -> c.Dependencies |> List.contains cell)
        let dirty = dependents @ dirtyCells
        
        // selecting ordered items based on dirtiness ensures no duplicate filtering is needed for dirty!
        let scheduled = order.Value 
                        |> List.collect (fun node -> 
                            match node with Graph.Node orderedCell -> 
                                            if dirty |> List.contains orderedCell 
                                            then [orderedCell] 
                                            else [])

        dirtyCells <- scheduled
        doUpdate()

    member private this.registerCell<'C when 'C :> ICell> (cell :'C) = 
        cell.Changed.Add(scheduleDependentCellUpdate cell)
        cells <- (cell :> ICell) :: cells
        order <- Lazy<List<Graph.Node<ICell>>>( calculateOrder )
        cell

    member r.createInputCell    num         = InputCell(num) |> r.registerCell
    member r.createComputeCell  inputs expr = ComputeCell(inputs, expr) |> r.registerCell
                                            
