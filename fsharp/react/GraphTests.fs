﻿module GraphTests

open FsUnit.Xunit
open Xunit

open React.Graph

[<Fact>]
let ``Topological sort of empty set returns an empty list`` () =
    let nodes = Set.empty<Node<int>>
    let edges = Set.empty<Edge<int>>

    topologicalSort nodes edges 
        |> should equal (Some List.empty<Node<int>>)


[<Fact>]
let ``Topological sort of graph with directed circle returns None`` () =
    let nodeA = Node "A"
    let nodeB = Node "B"
    let nodeC = Node "C"

    let nodes = Set.ofList [nodeA; nodeB; nodeC]
    let edges = Set.ofList [
        Edge(nodeA, nodeB);
        Edge(nodeA, nodeC);
        Edge(nodeB, nodeC);
        // the directed circle
        Edge(nodeC, nodeB);
    ]

    topologicalSort nodes edges |> should equal None

[<Fact>]
let ``Topological sort of DAG return a valid order`` () =
    let nodeA = Node "A"
    let nodeB = Node "B"
    let nodeC = Node "C"
    let nodeD = Node "D"
    let nodeE = Node "E"
    let nodeF = Node "F"    // free standing node
    
    let nodes = Set.ofList [nodeA; nodeB; nodeC; nodeD; nodeE; nodeF]
    let edges = Set.ofList [
        Edge(nodeA, nodeC);
        Edge(nodeB, nodeC);
        Edge(nodeB, nodeE);
        Edge(nodeC, nodeE);
        Edge(nodeD, nodeE);
    ]
    
    let result = topologicalSort nodes edges

    result |> Option.isSome |> should be True

    let sorted = Option.get result

    // All nodes are present exactly once in the list
    sorted |> should haveLength nodes.Count
    for node in nodes do 
        sorted |> should contain node 


    let containInOrder (early, late) = 
        let nodeIndex node collection = List.findIndex (fun n -> n = node) collection
        
        let hasInOrder earlier later collection = (collection |> nodeIndex earlier) < (collection |> nodeIndex later)
        
        let description = sprintf "Expected items to be in collection in order: %A anywhere before %A" early late 
       
        NHamcrest.Core.CustomMatcher<obj>(description, 
                                            fun c -> match c with 
                                                        | :? (Node<_> list) as collection -> collection |> hasInOrder early late
                                                        | _ -> false)

    sorted |> should containInOrder (nodeA, nodeC)
    sorted |> should containInOrder (nodeB, nodeC)
    sorted |> should containInOrder (nodeB, nodeE)
    sorted |> should containInOrder (nodeC, nodeE)
    sorted |> should containInOrder (nodeD, nodeE)
    